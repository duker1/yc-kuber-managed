resource "yandex_container_registry" "my-registry" {
  name = "diploma-registry"
  folder_id = var.yc_folder_id
  labels = {
    my-label = "for-my-apps"
  }
}