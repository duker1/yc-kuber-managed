# Variables

variable "yc_token" {
  type        = string
  description = "Yandex Cloud API key"
  default     =  "$yc_token"
}

variable "yc_cloud_id" {
  type        = string
  description = "Yandex Cloud id"
  default     = "$yc_cloud_id"
}

variable "yc_folder_id" {
  type        = string
  description = "Yandex Cloud folder id"
  default     =  "$yc_folder_id"
}

variable "yc_bucket1" {
  type        = string
  description = "Yandex Cloud API key"
  default     = "$yc_bucket"
}