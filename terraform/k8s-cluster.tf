resource "yandex_kubernetes_cluster" "cluster" {
 name = "cluster"
 network_id = yandex_vpc_network.k8s-network.id
 
 service_account_id      = yandex_iam_service_account.k8s-account.id
 node_service_account_id = yandex_iam_service_account.k8s-account.id
   depends_on = [
     yandex_resourcemanager_folder_iam_binding.editor,
     yandex_resourcemanager_folder_iam_binding.images-puller
   ]

 master {
   zonal {
     zone     = "${yandex_vpc_subnet.k8s-subnet-1.zone}"
     subnet_id = "${yandex_vpc_subnet.k8s-subnet-1.id}"
   }
    version   = "1.25"
    public_ip = true
 }
}
