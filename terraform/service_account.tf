# Service account create

resource "yandex_iam_service_account" "k8s-account" {
 name        = "k8s-account"
 description = "k8s-account description"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 # Сервисному аккаунту назначается роль "editor".
 folder_id = var.yc_folder_id
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-account.id}"
 ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
 # Сервисному аккаунту назначается роль "container-registry.images.puller".
 folder_id = var.yc_folder_id
 role      = "container-registry.images.puller"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-account.id}"
 ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-pusher" {
 # Сервисному аккаунту назначается роль "container-registry.images.puller".
 folder_id = var.yc_folder_id
 role      = "container-registry.images.pusher"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-account.id}"
 ]
}
