# Compute instance group for control-plane

resource "yandex_kubernetes_node_group" "k8s-masters" {
  name               = "kube-masters"
  cluster_id = yandex_kubernetes_cluster.cluster.id
  depends_on = [
    yandex_vpc_network.k8s-network,
    yandex_vpc_subnet.k8s-subnet-1,
    yandex_vpc_subnet.k8s-subnet-2,
    yandex_vpc_subnet.k8s-subnet-3,
  ]

  instance_template {
    name = "master-{instance.index}"
    platform_id = "standard-v2"
    resources {
      cores         = 2
      memory        = 4
      core_fraction = 100
    }

    scheduling_policy {
      preemptible = true  // Прерываемая ВМ
    }
    boot_disk {
      type = "network-ssd"
      size = 64
    }

    network_interface {
      
      subnet_ids = [
        yandex_vpc_subnet.k8s-subnet-1.id,
        yandex_vpc_subnet.k8s-subnet-2.id,
        yandex_vpc_subnet.k8s-subnet-3.id,
      ]
      nat = true
    }
    # metadata = {
    #   ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    # }
  }
  
  scale_policy {
    fixed_scale {
      size = 3
    }
  }
}

resource "yandex_kubernetes_node_group" "k8s-workers" {
  name               = "kube-workers"
  cluster_id = yandex_kubernetes_cluster.cluster.id
  depends_on = [
    yandex_vpc_network.k8s-network,
    yandex_vpc_subnet.k8s-subnet-1,
    yandex_vpc_subnet.k8s-subnet-2,
    yandex_vpc_subnet.k8s-subnet-3,
  ]

  instance_template {
    name = "worker-{instance.index}"
    platform_id = "standard-v2"
    resources {
      cores         = 2
      memory        = 4
      core_fraction = 100
    }

    
    scheduling_policy {
      preemptible = true  // Прерываемая ВМ
    }
    boot_disk {
      type = "network-ssd"
      size = 64
    }
    network_interface {
      subnet_ids = [
        yandex_vpc_subnet.k8s-subnet-1.id,
        yandex_vpc_subnet.k8s-subnet-2.id,
        yandex_vpc_subnet.k8s-subnet-3.id,
      ]
      nat = true
    }
    
    # metadata = {
    #   ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    # }
  }
  scale_policy {
    fixed_scale {
      size = 1
    }
  }
}