variable "yandex_cloud_id" {
  default = "$yc_cloud_id"
}

variable "yandex_folder_id" {
  default = "$yc_folder_id"
}

variable "a-zone" {
  default = "ru-central1-a"
}

variable "yc_token" {
  type        = string
  description = "Yandex Cloud API key"
  default     = "$yc_token"
}

variable "yc_cloud_id" {
  type        = string
  description = "Yandex Cloud id"
  default     = "$yc_cloud_id"
}

variable "yc_folder_id" {
  type        = string
  description = "Yandex Cloud folder id"
  default     =  "$yc_folder_id"
}