## Register in Yandex Cloud

https://cloud.yandex.ru

## Install Terraform client 
dfs
https://learn.hashicorp.com/terraform/getting-started/install

## Install Ansible

https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## Install Kubectl

https://kubernetes.io/docs/tasks/tools/install-kubectl/

## Install Helm

https://helm.sh/docs/intro/install/

### Описание проекта ##
```
./bucket/bucket.tf - установка бакета, где будет храниться наш бэкенд. Запускать первым
./bucket/provider.tf - провайдер Яндекса для создания S3 бакета
./bucket/variables.tf - описание переменных для бакет
Создание кластера с сохранением в ранее созданный бакет.
provider.tf - указание провайдера Яндекс для terraform
k8s-cluster.tf - создание кластера
k8s-network.tf - создание подсетей кластера
k8s-nodes.tf - создание master и worker нод кластера
service_account.tf - создание сервис аккаунта для управления кластером
vars.tf - описание переменных для terraform
```

Для запуска <br>
```bash
terraform init -backend-config=backend.key 
terraform plan
terraform apply -auto-approve
```
